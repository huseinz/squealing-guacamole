<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Events Stevents</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="index.css" rel="stylesheet">


</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/index.php">Events Stevents</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php">Dashboard</a></li>
                    <li><a href="#">Settings</a></li>
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Help</a></li>
                    <?php
              include 'login_lib.php';
              session_start();
              if(isLoggedIn()){
                $userLoggedIn = $_SESSION['name'];
                echo '<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="welcome">Welcome  ' . $userLoggedIn . '! </a>' .
                    '<div class="dropdown-menu" style="padding:15px;width:250px;">
                        <button type="button" id="btnLogout" class="btn" onclick="logout()">Logout</button>
                      </form>
                    </div></li>';
              }
              else{
                print(
                  '<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
                    <div class="dropdown-menu" style="padding:15px;width:250px;">
                      <!-- This is the Login form-->
                      <form class="form" id="formLogin">
                        <input name="username" id="username" type="text" placeholder="Enter Username" class="form-control"> 
                        <input name="password" id="password" type="password" placeholder="Enter Password" class="form-control"><br>
                        <div id="loginError"></div> 
                        <button type="button" id="btnLogin" class="btn btn-primary" onclick="validate()">Login</button>
                        <button type="button" id="btnRegister" class="btn" onclick="location.href=\'/register.php\';">Register</button>                      
                      </form>
                    </div>
                  </li>'
                );
              }
            ?>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid" style="max-width:1024px">
        <h1 class="well">Create Event</h1>
        <div class="col-sm-12 alert alert-danger" id="registerError" style="display:none;"></div>
        <div class="col-sm-12 alert alert-info" id="registerSuccess" style="display:none;"></div>
        <div class="col-lg-12 well">
            <div class="row">
                <form id="createevent">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label>Event Name</label>
                                <input type="text" name="eventname" id="eventname" class="form-control" required="required">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Date</label>
                                <input type="date" name="date" id="date" class="form-control" required="required">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label>Begin Time</label>
                                <input type="time" name="stime" id="stime" class="form-control" required="required">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>End Time</label>
                                <input type="time" name="etime" id="etime" class="form-control" required="required">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label>Phone Number</label>
                                <input type="tel" name="tel" id="tel" pattern="(?:\(\d{3}\)|\d{3})[- ]?\d{3}[- ]?\d{4}" class="form-control" required="required">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Requester Email</label>
                                <input type="email" name="email" id="email" class="form-control" required="required">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Event Theme (social, fundraising, talk, etc.)</label>
                                <input type="text" name="category" id="category" class="form-control" required="required">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Event Type</label>
                                <br>
                                <label class="radio-inline">
                                    <input type="radio" name="type" id="type" value="PUBLIC" checked required="required">Public</label>
                                <label class="radio-inline">
                                    <input type="radio" name="type" id="type" value="PRIVATE">Private</label>
                                <label class="radio-inline">
                                    <input type="radio" name="type" id="type" value="RSO">RSO</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label>Description</label>
                                <textarea rows="3" name="descr" id="descr" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 well" id="googlemap" style="height: 400px; margin-bottom:10px;"></div>
                            <div class="col-sm-6">
                                <div class="col-sm-12 form-group">
                                    <label>Location Description</label>
                                    <input type="text" name="location" id="locationdescr" class="form-control" required="required">
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>Location</label>
                                    <input type="text" id="location" class="form-control">
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Latitude</label>
                                    <input type="text" name="latitude" id="latitude" class="form-control" required="required">
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Longitude</label>
                                    <input type="text" name="longitude" id="longitude" class="form-control" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-lg btn-primary center-block">Submit</button>
                </form>
            </div>
        </div>


    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?libraries=places'></script>
    <script src="dist/js/locationpicker.jquery.js"></script>
    <script>
        $(document).ready(function () {
            console.log("ready!");

            $('#googlemap').locationpicker({
                    location: {
                        latitude: 28.6024274,
                        longitude: -81.20005989999999
                    },
                    radius: 0,
                    inputBinding: {
                        latitudeInput: $('#latitude'),
                        longitudeInput: $('#longitude'),
                        locationNameInput: $('#location')
                    },
                    enableAutocomplete: true,

                }

            );

            $("#createevent").submit(function (event) {
                console.log("In the submit function`");
                event.preventDefault();
                if (checkDateTime()) {
                    var data = $(this).serialize();
                    console.log(data);
                    $.post("/submitevent.php",
                        data,
                        function (response) {
                            console.log("In the call back function");
                            console.log(response);
                            if (parseInt(response) == 1) {
                                $("#registerError").hide();
                                $("#registerSuccess").attr('class', 'col-sm-12 alert alert-success');
                                $("#registerSuccess").text("Event Created Successfully");
                                $("#registerSuccess").fadeIn(1000).show();
                                $('#createevent')[0].reset();
                                //window.location = '/index.php';
                            } else {
                                $("#registerSuccess").hide();
                                $("#registerError").text("Submission Error: " + response);
                                $("#registerError").fadeIn(1000).show();

                            }
                        }
                    );
                }
            });
        });

        function checkDateTime() {
            var stime = $("#stime").val().split(":");
            var etime = $("#etime").val().split(":");
            stime = (parseInt(stime[0]) * 60) + parseInt(stime[1]);
            etime = (parseInt(etime[0]) * 60) + parseInt(etime[1]);
            console.log(stime);
            console.log(etime);

            if (etime < stime) {
                $("#registerError").text("Check your times");
                $("#registerError").fadeIn(1000).show();
                return false;
            } else if ((etime - stime) < 60) {
                $("#registerError").text("Event must be at least 1 hour long");
                $("#registerError").fadeIn(1000).show();
                return false;

            } else if ((etime - stime) > 360) {
                $("#registerError").text("Event must not be longer than 6 hours");
                $("#registerError").fadeIn(1000).show();
                return false;
            } else {
                $("#registerError").hide();
                return true;
            }
        }

        function validate() {
            console.log("validate Function called");
            var username = $.trim($("#username").val());
            var password = $.trim($("#password").val());
            if (username.length > 0 && password.length > 0) {
                console.log("calling login");
                login();
            } else {
                $("#loginError").text("One or more fields are empty");
            }
        }

        function login() {
            console.log("Login Function called");
            var username = $("#username").val();
            var password = $("#password").val();
            if (username != null && password != null) {
                $.post("/login.php", {
                        username: username,
                        password: password,
                        login: 1
                    },
                    function (data) {
                        console.log("In call back function");
                        console.log(data);
                        var result = parseInt(data);
                        if (result == 1) {
                            $("#loginError").text("Logging in....");
                            window.location = '/index.php';
                        } else {
                            // Way to indicate the credentials were wrong or does not exist.
                        }
                    }
                );
            }
        }

        function logout() {
            console.log("Logout Function called");
            $.post("/logout.php",
                function (data) {
                    console.log(data);
                    window.location = '/';
                });
        }
    </script>
</body>

</html>