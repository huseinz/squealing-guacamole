<?php
    header('Content-type: application/json');
    require 'connect.php';
    session_start();

	if(isset($_POST['name'])){
		$query = "SELECT * FROM event WHERE REPLACE(name, ' ', '') = '". preg_replace('/\s+/', '', $_POST['name']) . "'";
            	$response = $dbc->query($query);
            	$rows = array();
            	while($r = mysqli_fetch_assoc($response)) {
                	$rows[] = $r;
            	}
            	echo json_encode($rows);
	}

    if(isset($_SESSION['login'])){
        if(isset($_POST['time'])){
            if($_POST['time'] == 'today'){
                $query = "SELECT * FROM event WHERE date = CURDATE() AND (type = 'PUBLIC' OR type = 'PRIVATE') ORDER BY date asc, stime asc";

            }

            if($_POST['time'] == 'week'){
                $query = "SELECT * FROM event WHERE (type = 'PUBLIC' OR type = 'PRIVATE') AND date BETWEEN CURDATE() AND ADDDATE(CURDATE(), 7) ORDER BY date asc, stime asc";
            }

            if($_POST['time'] == 'month'){
                $query = "SELECT * FROM event WHERE (type = 'PUBLIC' OR type = 'PRIVATE') AND YEAR(CURDATE()) = YEAR(date) AND MONTH(CURDATE()) = MONTH(date) AND DAY(CURDATE()) <= DAY(date) ORDER BY date asc, stime asc";
            }


            $response = $dbc->query($query);
            $rows = array();
            while($r = mysqli_fetch_assoc($response)) {
                $rows[] = $r;
            }
            echo json_encode($rows);
        }
    }
    else{
        if(isset($_POST['time'])){
            if($_POST['time'] == 'today'){
                $query = "SELECT * FROM event WHERE date = CURDATE() AND type = 'PUBLIC' ORDER BY date asc, stime asc";

            }

            if($_POST['time'] == 'week'){
                $query = "SELECT * FROM event WHERE type = 'PUBLIC' AND date BETWEEN CURDATE() AND ADDDATE(CURDATE(), 7) ORDER BY date asc, stime asc";
            }

            if($_POST['time'] == 'month'){
                $query = "SELECT * FROM event WHERE type = 'PUBLIC' AND YEAR(CURDATE()) = YEAR(date) AND MONTH(CURDATE()) = MONTH(date) AND DAY(CURDATE()) <= DAY(date) ORDER BY date asc, stime asc";
            }


            $response = $dbc->query($query);
            $rows = array();
            while($r = mysqli_fetch_assoc($response)) {
                $rows[] = $r;
            }
            echo json_encode($rows);
        }
    }
?>