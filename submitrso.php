<?php
    require 'functions.php';
    require 'connect.php';
    session_start();

    //print_r($_POST);
    
    $rsoname = $_POST['rsoname'];
    $admin = $_POST['admin'];
    $members = $_POST['member'];

//    echo "rso: " . $rsoname . "<br/>";
//    echo "admin: " .$admin . "<br/>";
//    foreach ($members as $member) 
//    {
//        echo "$member <br>";
//    }
    
    // Check for emails that have been entered more than once
    $check = $members;
    $check[] = $admin;
    if(duplicates($check)){
        exit("An email has been entered more than once!");
    }
    
    // Prepare the query
    $stmt = $dbc->prepare("SELECT * FROM user WHERE email = ?");
    // Bind a var to the parameter
    $stmt->bind_param("s", $member);

    // Set the value of the var
    $member = $admin;
    
    // Execute the query
    $stmt->execute();
    
    // Count the number of results returned
    $stmt->store_result();
    if($stmt->num_rows < 1)
    {
        exit("User with that email does not exist!");
    }
    $stmt->free_result();
    
    // Check email of the members. Stops at first email that does not exist
    foreach ($members as $member)
    {
        // Execute the query
        $stmt->execute();

        // Count the number of results returned
        $stmt->store_result();
        if($stmt->num_rows < 1)
        {
            exit("Member with " . $member . " email does not exist!");
        }
        $stmt->free_result();
    }
    $stmt->close();


    // Check if all members are students
    // Prepare the query
    $stmt = $dbc->prepare("SELECT U.id FROM user AS U, student AS S WHERE U.id = S.pid AND U.email = ?");
    // Bind a var to the parameter
    $stmt->bind_param("s", $member);
    
    // Check email of the members. Stops at first email that does not exist
    foreach ($members as $member)
    {
        // Execute the query
        $stmt->execute();

        // Count the number of results returned
        $stmt->store_result();
        if($stmt->num_rows < 1)
        {
            exit("Member with " . $member . " email is not a student!");
        }
        $stmt->free_result();
    }
    $stmt->close();
    
    // Check if rso by name already exist
    $query = "SELECT * FROM rso WHERE name = '$rsoname'";
    $response = $dbc->query($query);
    if($response->num_rows > 0)
    {
        exit("RSO by " . $rsoname . " name already exists");
    }

    // Check if admin is already an admin
    $query = "SELECT U.id FROM user AS U, admin AS A WHERE U.id = A.aid AND U.email = '$admin'";
    $response = $dbc->query($query);

    // If they are not then check if admin is a student
    if($response->num_rows == 0)
    {
        $query = "SELECT U.id FROM user AS U, student AS S WHERE U.id = S.pid AND U.email = '$admin'";
        $response = $dbc->query($query);
        // If they are a student add them as admin
        if($response->num_rows > 0)
        {
            $query = "INSERT INTO admin (aid) SELECT id FROM user WHERE email = '$admin'";
            $response = $dbc->query($query);
        }
        else
        {
            exit("The admin is not a student!");    
        }
    }

    // Insert its admin, name into database
    $query = "INSERT INTO rso (name, aid) SELECT '$rsoname', id FROM user WHERE email = '$admin'";
    $response = $dbc->query($query);

    // Insert the members in now.
    // Prepare the query
    $stmt = $dbc->prepare("INSERT INTO member (pid, rid) SELECT U.id, R.rid FROM user AS U, rso AS R WHERE U.email = ? AND  R.name = '$rsoname'");
    // Bind a var to the parameter
    $stmt->bind_param("s", $member);
    // Insert Admin
    $member = $admin;
    $stmt->execute();

    // Insert the members.
    foreach ($members as $member)
    {
        // Execute the query
        $stmt->execute();

    }
    $stmt->close();
    
    exit("1");

//    This is how you display the results using fetch. Must have a var for every column
//    $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6);
//    $stmt->fetch();
//    printf("%s %s %s %s %s %s\n", $col1, $col2, $col3, $col4, $col5, $col6);

?>