<?php
    require 'functions.php';
    require 'connect.php';
    session_start();
    
    //print_r($_POST);

    $eventname = $_POST['eventname'];
    $date = $_POST['date'];
    $stime = $_POST['stime'] . ":00";
    $etime = $_POST['etime'] . ":00";
    $tel = $_POST['tel'];
    $email = $_POST['email'];
    $location = $_POST['location'];  
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];
    $category = $_POST['category'];
    $type = $_POST['type'];
    $descr = trim($_POST['descr']);
    $descr = !empty($descr) ? "'$descr'" : "NULL";
    
    

    // Check if event with name aleady exists.
    $query = "SELECT * FROM event WHERE name = '$eventname'";
    $response = $dbc->query($query);
    if($response->num_rows > 0)
    {
        exit("Event with name already exists: " . $eventname);
    }

    // Check if event already held at time and place
    $latbot = $latitude - 0.0001;
    $latup = $latitude + 0.0001;
    $longbot = $longitude - 0.0001;
    $longup = $longitude + 0.0001;
    $query = "SELECT * FROM event WHERE ((stime BETWEEN '$stime' AND '$etime') OR (etime BETWEEN '$stime' AND '$etime')) AND (date = '$date') AND (latitude BETWEEN $latbot AND $latup) AND (longitude BETWEEN $longbot AND $longup)";
    $response = $dbc->query($query);
    if($response->num_rows > 0)
    {
        exit("Event time and location conflicts with another event");
    }

    // Insert the event into database
    $query = "insert into event (name, descr, date, stime, etime, cphone, cemail, latitude, longitude, type, category, location) values ('$eventname', $descr, '$date', '$stime', '$etime', '$tel', '$email', $latitude, $longitude, '$type', '$category', '$location')";
    if ($dbc->query($query) === TRUE)
    {
        exit("1");
        exit;
    }
    else
    {
        echo "Could not submit event";
        exit;
    }

?>