<?php
    function duplicates($array) 
    {
       return count($array) !== count(array_unique($array));
    }
?>