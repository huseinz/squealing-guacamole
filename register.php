<?php
if(isset($_POST['register'])){
    $usern = $_POST['username'];
    $passw = $_POST['password'];
    $email = $_POST['email'];
    $fname = $_POST['fName'];
    $lname = $_POST['lName'];

    require 'connect.php';
    $query = "SELECT * FROM user WHERE username = '$usern' OR email = '$email'";
    $response = $dbc->query($query);

    if($response->num_rows > 0){
      echo "A user by that name or email already exists";
      exit;
    }
    else{
      $query = "INSERT INTO user (username, password, firstName, lastName, email) VALUES ('$usern', '$passw', '$fname', '$lname', '$email');";
      if ($dbc->query($query) === TRUE){
        echo 1;
        exit;
      }
      else{
        echo "Could not enter user into database";
        exit;

      }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Events Stevents</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="index.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- ><script src="../../assets/js/ie-emulation-modes-warning.js"></script><-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Events Stevents</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
              <div class="dropdown-menu" style="padding:15px;width:250px;">
                <!-- This is the Login form-->
                <form class="form" id="formLogin">
                  <input name="username" id="username" type="text" placeholder="Enter Username" class="form-control"> 
                  <input name="password" id="password" type="password" placeholder="Enter Password" class="form-control"><br>
                  <div id="loginError"></div> 
                  <button type="button" id="btnLogin" class="btn btn-primary" onclick="validate()">Login</button>
                  <button type="button" id="btnRegister" class="btn" onclick="location.href=\'/register.php\';">Register</button>                      
                </form>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="max-width:768px;">
      <h1 class="well">Registration Form</h1>
      <div class="col-sm-12 alert alert-danger" id="registerError" style="display:none;"></div>
      <div class="col-sm-12 alert alert-info" id="registerSuccess" style="display:none;"></div>
      <div class="col-lg-12 well">
      <div class="row">
            <form id="register">
              <div class="col-sm-12">
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label>First Name</label>
                    <input type="text" id="fName" placeholder="Enter First Name Here.." class="form-control">
                  </div>
                  <div class="col-sm-6 form-group">
                    <label>Last Name</label>
                    <input type="text" id="lName" placeholder="Enter Last Name Here.." class="form-control">
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4 form-group">
                    <label>Username</label>
                    <input type="text" id="rusername" placeholder="Enter Username Here.." class="form-control">
                  </div>
                  <div class="col-sm-4 form-group">
                    <label>Password</label>
                    <input type="password" id="rpassword" placeholder="Enter Password Here.." class="form-control">
                  </div>
                  <div class="col-sm-4 form-group">
                    <label>Confirm Password</label>
                    <input type="password" id="cpassword" placeholder="Confirm Password Here.." class="form-control">
                  </div>
                </div>                        
                <div class="form-group">
                  <label>Email Address</label>
                  <input type="email" id="email" placeholder="Enter Email Address Here.." class="form-control">
                </div>  
                <div class="form-group">
                  <label>University</label>
                  <input type="text" id="university" placeholder="Enter University Name Here.." class="form-control">
                </div>  
              <button type="submit" class="btn btn-lg btn-info">Submit</button>         
              </div>
            </form> 
            </div>
      </div>
      
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.3.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function(){
          console.log( "ready!" );

          //This is the document submit function.
          $("#register").submit(function( event ) {
            // body...
            var rusername = $.trim($("#rusername").val());
            var rpassword = $.trim($("#rpassword").val());
            var cpassword = $.trim($("#cpassword").val());
            var lName = $.trim($("#lName").val());
            var fName = $.trim($("#fName").val());
            var email = $.trim($("#email").val());

            // Check if Fields are filled
            if(rusername.length > 0 && 
               rpassword.length > 0 && 
               cpassword.length > 0 &&
               fName.length > 0 &&
               lName.length > 0 &&
               email.length > 0){
              //Check if passwords match
              if (cpassword == rpassword) {
                event.preventDefault();
                //Send data to server
                console.log("Sending Data");
                $("#registerError").hide();
                $("#registerSuccess").fadeIn(1000).show();
                $("#registerSuccess").text("Sending Data.....");

                $.post("/register.php",
                  {
                    username: rusername,
                    password: rpassword,
                    fName: fName,
                    lName: lName,
                    email: email,
                    register: 1
                  },
                  function (response) {
                    console.log("In register call back function");
                    console.log(response);
                    if(parseInt(response) == 1){
                      $("#registerSuccess").attr('class', 'col-sm-12 alert alert-success');
                      $("#registerSuccess").text("Success!");
                      window.location = '/index.php';
                    }
                    else{
                        $("#registerSuccess").hide();
                        $("#registerError").text("Registration Error: " + response);
                        $("#registerError").fadeIn(1000).show();

                    }
                  }
                );

              }
              else {
                $("#registerError").fadeIn(1000).show();
                $("#registerError").text("Passwords Dont Match");
                event.preventDefault();
              }
            }
          else{
            $("#registerError").fadeIn(1000).show();
            $("#registerError").text("One or more fields are empty");
            event.preventDefault();
          }
          })          
      });

      function validate(){
        console.log( "validate Function called" );
        var username = $.trim($("#username").val());
        var password = $.trim($("#password").val());
        if(username.length > 0 && password.length > 0){
           console.log( "calling login" );
          login();
        }
        else{
          $("#loginError").text("One or more fields are empty");
        }
      }

      function login(){
        console.log( "Login Function called" );
        var username = $("#username").val();
        var password = $("#password").val();
        if(username != null && password != null){
          $.post("/login.php",
              {
                username: username,
                password: password,
                login: 1
              },
              function(data){
                console.log( "In call back function" );
                console.log(data);
                var result = parseInt(data);
                if(result == 1){
                  $("#loginError").text("Logging in....");
                  window.location = '/index.php';
                }
                else{
                  $("#loginError").text("The Username or Password you input is invalid.");
                }
              }
          );
        }
      }

      function logout(){
        console.log( "Logout Function called" );
        $.post("/logout.php",
          function(data){
            console.log(data);
            window.location = '/';
          });
      }
    </script>
  </body>
</html>
