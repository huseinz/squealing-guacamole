<?php
    if(isset($_GET['id'])){
        $id = $_GET['id'];
    }
    else{
        exit("There was no event selected");
    }
    require 'connect.php';

    $query = "SELECT * FROM event WHERE eid = '$id'";
    $response = $dbc->query($query);
    if($response !== FALSE)
    {
        if($response->num_rows > 0)
        {
            $row = $response->fetch_assoc();
            $eid = $row['eid'];
            $name = $row['name'];
            $descr = $row['descr'];
            $date = $row['date'];
            $stime = $row['stime'];
            $etime = $row['etime'];
            $cphone = $row['cphone'];
            $cemail = $row['cemail'];
            $location = $row['location'];
            $latitude = $row['latitude'];
            $longitude = $row['longitude'];
            $category = $row['category'];
            $type = $row['type'];
        }   
    }
   
    
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Events Stevents</title>

        <!-- Bootstrap core CSS -->
        <link href="dist/css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="index.css" rel="stylesheet">

    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/index.php">Events Stevents</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php">Dashboard</a></li>
                        <li><a href="#">Profile</a></li>
                        <?php
              include 'login_lib.php';
              session_start();
              if(isLoggedIn()){
                $userLoggedIn = $_SESSION['name'];
                echo '<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="welcome">Welcome  ' . $userLoggedIn . '! </a>' .
                    '<div class="dropdown-menu" style="padding:15px;width:250px;">
                        <button type="button" id="btnLogout" class="btn" onclick="logout()">Logout</button>
                      </form>
                    </div></li>';
              }
              else{
                print(
                  '<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
                    <div class="dropdown-menu" style="padding:15px;width:250px;">
                      <!-- This is the Login form-->
                      <form class="form" id="formLogin">
                        <input name="username" id="username" type="text" placeholder="Enter Username" class="form-control"> 
                        <input name="password" id="password" type="password" placeholder="Enter Password" class="form-control"><br>
                        <div id="loginError"></div> 
                        <button type="button" id="btnLogin" class="btn btn-primary" onclick="validate()">Login</button>
                        <button type="button" id="btnRegister" class="btn" onclick="location.href=\'/register.php\';">Register</button>                      
                      </form>
                    </div>
                  </li>'
                );
              }
            ?>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid" style="max-width:1024px">
            <div class="col-sm-12 well">
                <div class="row col-sm-12" style="padding:0px">
                    <h1 class="col-sm-12"><?php echo $name; ?></h1>
                    <h2 class="col-sm-4"><?php echo $date; ?></h2>
                    <h2 class="col-sm-4"><?php echo $stime . " - " . $etime; ?></h2>
                </div>

                <div class="row" col-sm-12>
                    <p class="col-sm-12">
                        <?php echo $descr; ?>
                    </p>
                </div>

                <div class="row" col-sm-12>
                    <div class="col-sm-6">
                        <h4 style="margin:0px; padding:0px">Location</h4>
                        <div id="map" class="col-sm-12" style="height: 400px; margin-bottom:10px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-12" style="margin-bottom:15px">
                            <h4 style="margin:0px; padding:0px">Contact Info</h4>
                            <span class="col-sm-12" style="padding:0px">Email: <?php echo $cphone; ?></span>
                            <span class="col-sm-12" style="padding:0px">Phone: <?php echo $cemail; ?></span>
                        </div>
                        <br>
                        <div class="col-sm-12" style="margin-bottom:15px">
                            <h4 style="margin:0px; padding:0px">Category</h4>
                            <span class="col-sm-12" style="padding:0px"><?php echo $category; ?></span>

                        </div>

                    </div>
                </div>
                <div class="row col-sm-12">
                    <h4>Comments</h4>
                    <?php
				$query = "SELECT * FROM comment WHERE eid = '$id'";
				$response = $dbc->query($query);
				if($response !== FALSE){
					if($response->num_rows > 0){
						$numcomments = $response->num_rows;
					}
				} 
				for($i = 0; $i < $numcomments; $i++){
					$row = mysqli_fetch_array($response, MYSQLI_NUM);
					$query = "SELECT * FROM user WHERE id = '$row[2]'";
					$u = $dbc->query($query);
					$un = mysqli_fetch_array($u, MYSQLI_NUM);	
					printf('
						<div class="well col-sm-12">
							<div class="col-md-3">
							<h4>%s</h4> 
							%s
							</div>
							<div class="col-md-3">
							%s
							</div>
							<div class="col-md-6">
								<div class="pull-right">
								<button type="button">Edit</button>
								<button type="button">Delete</button>
							</div>
							</div>
						</div>
					',$un[1], $row[4], $row[3]);
	
				}			
			?>
                </div>
                <?php
                    if(isLoggedIn()){
                        print('<div class="row col-sm-12">
                        <form id="addcomment">
                            <div class="col-sm-12 ">
                                <textarea rows="3" name="comment" id="comment" class="form-control"></textarea>
                                <button type="button" onclick="sendcomment()">Post</button>
                            </div>
                        </form>
                        </div>'
                        );
                    }
                    else{
                        print(
                        '<div>Need to be logged in to post comments</div>'
                        );
                    }
                ?>

            </div>
        </div>
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src=" https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js "></script>
        <script src="dist/js/bootstrap.min.js "></script>
        <script>
            $(document).ready(function () {
                console.log("ready! ");


            });

            function validate() {
                console.log("validate Function called ");
                var username = $.trim($("#username ").val());
                var password = $.trim($("#password ").val());
                if (username.length > 0 && password.length > 0) {
                    console.log("calling login ");
                    login();
                } else {
                    $("#loginError ").text("One or more fields are empty ");
                }
            }

            function login() {
                console.log("Login Function called ");
                var username = $("#username ").val();
                var password = $("#password ").val();
                if (username != null && password != null) {
                    $.post("/login.php ", {
                            username: username,
                            password: password,
                            login: 1
                        },
                        function (data) {
                            console.log("In call back function ");
                            console.log(data);
                            var result = parseInt(data);
                            if (result == 1) {
                                $("#loginError ").text("Logging in.... ");
                                window.location = '/index.php';
                            } else {
                                $("#loginError ").text("Login Info is Incorrect ");
                            }
                        }
                    );
                }
            }

            function logout() {
                console.log("Logout Function called ");
                $.post("/logout.php ",
                    function (data) {
                        console.log(data);
                        window.location = '/';
                    });
            }

            function sendcomment() {
                var comment = $.trim($("#comment").val());
                var eid = <?php echo $eid; ?>;
                var userid = <?php echo $_SESSION['user_id']; ?>;
                $.post("/sendcomment.php", {
                        comment: comment,
                        eventid: eid,
                        userid: userid
                    },
                    function (data) {
                        console.log(data);
                    })
            }
        </script>
    </body>

    </html>