<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="plant.ico">

    <title>Events Stevents</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="index.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- ><script src="../../assets/js/ie-emulation-modes-warning.js"></script><-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Dashboard.css link below -->
    <link rel="stylesheet" type="text/css" href="index.css" media="screen" />

</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/index.php">Events Stevents</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a onclick="getEvents('today')">Today</a></li>
                    <li><a onclick="getEvents('week')">This Week</a></li>
                    <li><a onclick="getEvents('month')">This Month</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php">Dashboard</a></li>
                    <li><a href="#">Profile</a></li>
                    <?php
              include 'login_lib.php';
              session_start();
              if(isLoggedIn()){
                $userLoggedIn = $_SESSION['name'];
                echo '<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="welcome">Welcome  ' . $userLoggedIn . '! </a>' .
                    '<div class="dropdown-menu" style="padding:15px;width:250px;">
                        <button type="button" id="btnLogout" class="btn" onclick="logout()">Logout</button>
                      </form>
                    </div></li>';
              }
              else{
                print(
                  '<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
                    <div class="dropdown-menu" style="padding:15px;width:250px;">
                      <!-- This is the Login form-->
                      <form class="form" id="formLogin">
                        <input name="username" id="username" type="text" placeholder="Enter Username" class="form-control"> 
                        <input name="password" id="password" type="password" placeholder="Enter Password" class="form-control"><br>
                        <div id="loginError"></div> 
                        <button type="button" id="btnLogin" class="btn btn-primary" onclick="validate()">Login</button>
                        <button type="button" id="btnRegister" class="btn" onclick="location.href=\'/register.php\';">Register</button>                      
                      </form>
                    </div>
                  </li>'
                );
              }
            ?>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul id="event-sidebar" class="nav nav-sidebar">
                    <li class="active"><a><span style="font-weight:bold">Events Home</span><span class="sr-only">(current)</span></a></li>
                    <li><a onclick="getEvents('today')"><span style="font-weight:bold">Today</span></a></li>
                    <li><a onclick="getEvents('week')"><span style="font-weight:bold">This Week</span></a></li>
                    <li><a onclick="getEvents('month')"><span style="font-weight:bold">This Month</span></a></li>
                </ul>
                <ul class="nav nav-sidebar">
                    <li><a href="/create_rso_2.php"><span style="font-weight:bold">Create RSO</span></a></li>
                    <li><a href="/join_rso.php"><span style="font-weight:bold">Join RSO</span></a></li>
                    <li><a href="/create_event.php"><span style="font-weight:bold">Create Event</span></a></li>
                    <!--        <li><a href=""><span style="font-weight:bold">One more nav</span></a></li>
            <li><a href=""><span style="font-weight:bold">Another nav item</span></a></li> -->
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" id="main">
            </div>
        </div>
    </div>
    <!-- Test Comment -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            console.log("ready!");
            getEvents('today');

        });

        $('.nav-sidebar').on('click', 'li', function () {
            $(this).addClass('active').siblings().removeClass('active');
        });

        function validate() {
            console.log("validate Function called");
            var username = $.trim($("#username").val());
            var password = $.trim($("#password").val());
            if (username.length > 0 && password.length > 0) {
                console.log("calling login");
                login();
            } else {
                $("#loginError").text("One or more fields are empty");
            }
        }

        function login() {
            console.log("Login Function called");
            var username = $("#username").val();
            var password = $("#password").val();
            if (username != null && password != null) {
                $.post("/login.php", {
                        username: username,
                        password: password,
                        login: 1
                    },
                    function (data) {
                        console.log("In call back function");
                        console.log(data);
                        var result = parseInt(data);
                        if (result == 1) {
                            $("#loginError").text("Logging in....");
                            window.location = '/index.php';
                        } else {
                            $("#loginError").text("The Username or Password you input is invalid.");
                        }
                    }
                );
            }
        }

        function logout() {
            console.log("Logout Function called");
            $.post("/logout.php",
                function (data) {
                    console.log(data);
                    window.location = '/';
                });
        }

        function getEvents(time) {
            console.log(time);
            $.post("/event.php", {
                    time: time
                },
                function (data) {
                    $("#main").html("");
                    $.each(data, function () {
                        console.log(this);
                        console.log("Calling displayEvents");
                        displayEvents(this);
                    });
                },
                "json"
            );

        }


        function displayEvents(eventObj) {
            console.log("displayEvents called");
            $("#main").append(
                "<div class=\"well well-sm\" id=\"" + eventObj.name.replace(/\s+/g, '') + "\">" +
                "<h3>" + "<a href=\"/viewevent.php?id=" + eventObj.eid + "\">" + eventObj.name + "</a>" + "</h3>" + eventObj.descr + "</div>"
            );
        }


        function displayEventDetails(eventObj) {
            console.log("displayEventDetails called");
            $("#main").html(
                "<p>" + eventObj.name + "<br>" + eventObj.descr + "<br>" + eventObj.date + "<br>" + eventObj.time + "<br>" + eventObj.location + "<br>" + eventObj.category + "<br>" + eventObj.type + "<br>" + "</p>"
            );
        }
    </script>
</body>

</html>
