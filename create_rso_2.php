<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Events Stevents</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="index.css" rel="stylesheet">

</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/index.php">Events Stevents</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php">Dashboard</a></li>
                    <li><a href="#">Profile</a></li>
                    <?php
              include 'login_lib.php';
              session_start();
              if(isLoggedIn()){
                $userLoggedIn = $_SESSION['name'];
                echo '<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="welcome">Welcome  ' . $userLoggedIn . '! </a>' .
                    '<div class="dropdown-menu" style="padding:15px;width:250px;">
                        <button type="button" id="btnLogout" class="btn" onclick="logout()">Logout</button>
                      </form>
                    </div></li>';
              }
              else{
                print(
                  '<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
                    <div class="dropdown-menu" style="padding:15px;width:250px;">
                      <!-- This is the Login form-->
                      <form class="form" id="formLogin">
                        <input name="username" id="username" type="text" placeholder="Enter Username" class="form-control"> 
                        <input name="password" id="password" type="password" placeholder="Enter Password" class="form-control"><br>
                        <div id="loginError"></div> 
                        <button type="button" id="btnLogin" class="btn btn-primary" onclick="validate()">Login</button>
                        <button type="button" id="btnRegister" class="btn" onclick="location.href=\'/register.php\';">Register</button>                      
                      </form>
                    </div>
                  </li>'
                );
              }
            ?>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid" style="max-width:1024px">
        <h1 class="well">Registered Student Organization Creation Form</h1>
        <div class="col-sm-12 alert alert-danger" id="registerError" style="display:none;"></div>
        <div class="col-sm-12 alert alert-info" id="registerSuccess" style="display:none;"></div>
        <div class="col-sm-12 well">
            <form id="createrso">
                <!--                    action="submitrso.php" method="post"-->
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label>RSO Name</label>
                        <input type="text" name="rsoname" id="rsoname" class="form-control" required="required">
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Admin Email</label>
                        <input type="email" name="admin" id="admin" class="form-control" required="required">
                    </div>
                    <div class="members col-sm-8 form-group">
                        <label>Members</label>
                        <div class="entry form-group">
                            <input class="form-control" name="member[]" type="email" placeholder="Enter Member Email" required="required" />

                        </div>
                        <div class="entry form-group">
                            <input class="form-control" name="member[]" type="email" placeholder="Enter Member Email" required="required" />

                        </div>
                        <div class="entry form-group">
                            <input class="form-control" name="member[]" type="email" placeholder="Enter Member Email" required="required" />

                        </div>
                        <div class="entry input-group">
                            <input class="form-control" name="member[]" type="email" placeholder="Enter Member Email" required="required" />
                            <span class="input-group-btn">
                                <button class="btn btn-success btn-add" type="button">
                                    <span class="glyphicon glyphicon-plus"></span>
                            </button>
                            </span>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-lg btn-primary center-block">Submit</button>
            </form>
        </div>


    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            console.log("ready!");

            $("#createrso").submit(function (event) {
                console.log("In the submit function`");
                event.preventDefault();
                var data = $(this).serialize();
                console.log(data);
                $.post("/submitrso.php",
                    data,
                    function (response) {
                        console.log("In the call back function")
                        console.log(response);
                        if (parseInt(response) == 1) {
                            $("#registerError").hide();
                            $("#registerSuccess").attr('class', 'col-sm-12 alert alert-success');
                            $("#registerSuccess").text("You have successfully submitted your RSO!");
                            $("#registerSuccess").fadeIn(1000).show();
                            //window.location = '/index.php';
                        } else {
                            $("#registerSuccess").hide();
                            $("#registerError").text("Creation Error: " + response);
                            $("#registerError").fadeIn(1000).show();

                        }
                    }
                );
            });


            $(document).on('click', '.btn-add', function (e) {
                e.preventDefault();

                var controlForm = $('.members:first'),
                    currentEntry = $(this).parents('.entry:first'),
                    newEntry = $(currentEntry.clone()).appendTo(controlForm);

                newEntry.find('input').val('');
                controlForm.find('.entry:not(:last) .btn-add')
                    .removeClass('btn-add').addClass('btn-remove')
                    .removeClass('btn-success').addClass('btn-danger')
                    .html('<span class="glyphicon glyphicon-minus"></span>');
            }).on('click', '.btn-remove', function (e) {
                $(this).parents('.entry:first').remove();

                e.preventDefault();
                return false;
            });


        });

        function addMember() {
            $(".member-email:first").clone().appendTo("#members");
        }

        function validate() {
            console.log("validate Function called");
            var username = $.trim($("#username").val());
            var password = $.trim($("#password").val());
            if (username.length > 0 && password.length > 0) {
                console.log("calling login");
                login();
            } else {
                $("#loginError").text("One or more fields are empty");
            }
        }

        function login() {
            console.log("Login Function called");
            var username = $("#username").val();
            var password = $("#password").val();
            if (username != null && password != null) {
                $.post("/login.php", {
                        username: username,
                        password: password,
                        login: 1
                    },
                    function (data) {
                        console.log("In call back function");
                        console.log(data);
                        var result = parseInt(data);
                        if (result == 1) {
                            $("#loginError").text("Logging in....");
                            window.location = '/index.php';
                        } else {
                            $("#loginError").text("Login Info is Incorrect");
                        }
                    }
                );
            }
        }

        function logout() {
            console.log("Logout Function called");
            $.post("/logout.php",
                function (data) {
                    console.log(data);
                    window.location = '/';
                });
        }
    </script>
</body>

</html>